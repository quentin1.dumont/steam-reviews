import org.apache.spark.sql.{SparkSession, functions}

object HiddenInChina {
  def main (args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("Steam Reviews").getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    val file = spark.read
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(args(0))
    import spark.implicits._
    val filtered = file.filter($"hidden_in_steam_china".isin("0", "1") && $"game".isNotNull && $"game" =!= "")
    val result = filtered
      .groupBy($"game")
      .pivot("hidden_in_steam_china", Seq("0", "1"))
      .count()
      .na.fill(0)
    val gamesHiddenInChina = result.filter($"0" === 0).sort($"1".desc).collect()
    println(s"Games probably hidden in China: ${gamesHiddenInChina.length}")
    gamesHiddenInChina.foreach(row => println(s"${row.getString(0)}@|@${row.getLong(2)}"))
    /*val reviewsHiddenInChina = result.filter($"0" =!= 0).select($"1").agg(functions.sum($"1")).collect()(0)(0)
    println(s"Reviews hidden in China (games not hidden): $reviewsHiddenInChina")*/
    spark.stop()
  }
}
