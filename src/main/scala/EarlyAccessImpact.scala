import org.apache.spark.sql.functions.{when, sum}
import org.apache.spark.sql.SparkSession

object EarlyAccessImpact {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("Steam Reviews").getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    val file = spark.read
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(args(0))
    import spark.implicits._
    val filtered = file.filter($"game".isNotNull && $"game" =!= "" && $"voted_up".isin("0", "1")
      && $"written_during_early_access".isin("0", "1"))
    // Global
    filtered.groupBy($"written_during_early_access").pivot("voted_up").count().show()
    // Per game (including DLCs)
    val result = filtered
      .groupBy($"game")
      .agg(
        sum(when($"written_during_early_access" === "0" && $"voted_up" === "0", 1).otherwise(0)).as("voted_down_after_early_access"),
        sum(when($"written_during_early_access" === "0" && $"voted_up" === "1", 1).otherwise(0)).as("voted_up_after_early_access"),
        sum(when($"written_during_early_access" === "1" && $"voted_up" === "0", 1).otherwise(0)).as("voted_down_during_early_access"),
        sum(when($"written_during_early_access" === "1" && $"voted_up" === "1", 1).otherwise(0)).as("voted_up_during_early_access")
      )
      .orderBy($"game")
    result.collect().foreach(row => println(s"${row.getString(0)}|${row.getLong(1)}|${row.getLong(2)}|${row.getLong(3)}|${row.getLong(4)}"))
    spark.stop()
  }
}
