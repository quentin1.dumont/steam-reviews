import org.apache.spark.sql.{Row, SparkSession}

object MostVotedUpReviews {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("Steam Reviews").getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    val file = spark.read
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(args(0))
    import spark.implicits._
    file.sort($"votes_up".cast("Int").desc)
      .take(3)
      .foreach(printMostVotedUp)
    spark.stop()
  }

  private def printMostVotedUp(row: Row): Unit = {
    println(s"${row.getString(3)} said ${row.getString(11)} for ${row.getString(2)}")
    println(s"Votes up: ${row.getString(15)}, Time played: ${row.getString(6)} minutes")
    println()
  }
}
