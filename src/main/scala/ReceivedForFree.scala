import org.apache.spark.sql.functions.{sum, when}
import org.apache.spark.sql.{Row, SparkSession}

object ReceivedForFree {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("Steam Reviews").getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    val file = spark.read
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(args(0))
    import spark.implicits._
    val filtered = file.filter($"game".isNotNull && $"game" =!= "" && $"received_for_free".isin("0", "1")
      && $"voted_up".isin("0", "1"))
    // Global
    filtered.groupBy($"received_for_free").pivot("voted_up").count().show()
    // Per game (including DLCs)
    val result = filtered
      .groupBy($"game")
      .agg(
        sum(when($"received_for_free" === "0" && $"voted_up" === "0", 1).otherwise(0)).as("voted_down_not_free"),
        sum(when($"received_for_free" === "0" && $"voted_up" === "1", 1).otherwise(0)).as("voted_up_not_free"),
        sum(when($"received_for_free" === "1" && $"voted_up" === "0", 1).otherwise(0)).as("voted_down_free"),
        sum(when($"received_for_free" === "1" && $"voted_up" === "1", 1).otherwise(0)).as("voted_up_free")
      )
      .orderBy($"game")
    result.collect().foreach(row => println(s"${row.getString(0)}|${row.getLong(1)}|${row.getLong(2)}|${row.getLong(3)}|${row.getLong(4)}"))
    spark.stop()
  }
}
