import org.apache.spark.sql.Row

// Couldn't use the case class because of parsing errors on some rows in the review field

case class Review(recommendationid: Int, appid: Int, game: String, author_steamid: String, author_num_games_owned: Int,
                  author_num_reviews: Int, author_playtime_forever: Int, author_playtime_last_two_weeks: Int,
                  author_playtime_at_review: Int, author_last_played: Int, language: String, review: String,
                  timestamp_created: Int, timestamp_updated: Int, voted_up: Boolean, votes_up: Int, votes_funny: Int,
                  weighted_vote_score: Double, comment_count: Int, steam_purchase: Boolean, received_for_free: Boolean,
                  written_during_early_access: Boolean, hidden_in_steam_china: Boolean)

object Review {
  def fromRow(row : Row) : Review = {
    Review(
      row.getString(0).toInt, // recommendationid
      row.getString(1).toInt, // appid
      row.getString(2), // game
      row.getString(3), // author_steamid
      row.getString(4).toInt, // author_num_games_owned
      row.getString(5).toInt, // author_num_reviews
      row.getString(6).toInt, // author_playtime_forever
      row.getString(7).toInt, // author_playtime_last_two_weeks
      row.getString(8).toInt, // author_playtime_at_review
      row.getString(9).toInt, // author_last_played
      row.getString(10), // language
      row.getString(11), // review
      row.getString(12).toInt, // timestamp_created
      row.getString(13).toInt, // timestamp_updated
      intToBool(row.getString(14).toInt), // voted_up
      row.getString(15).toInt, // votes_up
      row.getString(16).toInt, // votes_funny
      row.getString(17).toDouble, // weighted_vote_score
      row.getString(18).toInt, // comment_count
      intToBool(row.getString(19).toInt), // steam_purchase
      intToBool(row.getString(20).toInt), // received_for_free
      intToBool(row.getString(21).toInt), // written_during_early_access
      intToBool(row.getString(22).toInt) // hidden_in_steam_china
    )
  }

  private def intToBool(value: Int): Boolean = value match {
    case 1 => true
    case 0 => false
  }
}
