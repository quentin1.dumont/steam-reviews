import org.apache.spark.sql.SparkSession

object LanguagesHiddenInChina {
  def main (args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("Steam Reviews").getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    val file = spark.read
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(args(0))
    import spark.implicits._
    val filtered = file.filter($"hidden_in_steam_china".isin("0", "1") && $"language".isNotNull && $"language" =!= "")
    filtered
      .groupBy($"language")
      .pivot("hidden_in_steam_china", Seq("0", "1"))
      .count()
      .na.fill(0)
      .show(40)
    spark.stop()
  }
}
